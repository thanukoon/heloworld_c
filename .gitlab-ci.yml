stages:
  - build
  - package
  - review
  - master
  - staging
  - production

## Define the deployctl var's
variables:
  CI_PROJECT_PATH_SLUG: "deployctl-heloworld-c"
  DEPLOY_DOMAIN_APP: "gioxapp.com"
  DEPLOY_DOMAIN: "hello-world.deployctl.com"
  DEPLOY_CONFIG_HTTPS: "True"
  MAIN_REPO_URL: "http://production.deployctl-repo-hello-world.gioxapp.com"
  MASTER_REPO_URL: "http://production.deployctl-bleeding-repo-hello-world.gioxapp.com"


# Building the Binaries
.build: &build
  stage: build
  image: dockcross/${CI_JOB_NAME}
  script: 
    - mkdir -pv output/bin
    - $CC helloworld.c -o output/bin/helloworld-${CI_JOB_NAME}
    - 'echo -e "$(git describe --tags --abbrev=8 | sed -e s/g// -e s/\-/\./g)" > .VERSION'
  artifacts:
    paths:
      - output/bin/
      - .VERSION

linux-x86: *build
linux-x64: *build
linux-armv6: *build
linux-armv7: *build
linux-arm64: *build
windows-x86: *build
windows-x64: *build
linux-ppc64le: *build

# Package RPM's

## RPM lookup Architectures versus Binary Architecture
.rpm_arch: &rpmarch >
  export rpm_i386=linux-x86;
  export rpm_x86_64=linux-x64;
  export rpm_arm=linux-armv6;
  export rpm_armv7hl=linux-armv7;
  export rpm_aarch64=linux-arm64;
  export rpm_ppc64le=linux-ppc64le;
  
## define the rpm packaging
.rpm: &rpm
  stage: package
  image: registry.gitlab.com/gitlab-org/gitlab-ci-multi-runner:ci-1.8
  script: 
    - export VERSION=$(cat .VERSION)
    - echo $VERSION
    - *rpmarch
    - 'eval BIN_ARCH="\${$CI_JOB_NAME}" && export BIN_ARCH'
    - export PACKAGE_ARCH="${CI_JOB_NAME:4}"
    - 'echo " using binary arch $BIN_ARCH"'
    - 'echo " for rpm arch: $PACKAGE_ARCH"'
    - rm -rf output/rpm && mkdir -pv output/rpm
    - > 
       fpm -s dir -t rpm
       -n helloworld
       -v ${VERSION}
       -p output/rpm/helloworld-${VERSION}-1.${PACKAGE_ARCH}.rpm
       --rpm-compression bzip2 --rpm-os linux
       --force
       --url https://gitlab.com/deployctl/heloworld_c
       --description "helloworld"
       -m "Gioxa Ltd Inc. <info@gioxa.com>"
       --license "MIT"
       --provides helloworld
       --replaces helloworld
       -a ${PACKAGE_ARCH}
       output/bin/helloworld-${BIN_ARCH}=/usr/bin/helloworld
  artifacts:
    paths:
      - output/rpm/

# package RPM's
rpm_i386: *rpm
rpm_x86_64: *rpm
rpm_arm: *rpm
rpm_armv7hl: *rpm
rpm_aarch64: *rpm
rpm_ppc64le: *rpm

# Package the .deb's

## deb lookup Architectures versus Binary Architecture
.deb_arch: &debarch >
  export deb_i386=linux-x86;
  export deb_amd64=linux-x64;
  export deb_armel=linux-armv6;
  export deb_armhf=linux-armv7;
  export deb_arm64=linux-arm64;
  export deb_ppc64le=linux-ppc64le;

## define the deb packaging
.deb: &deb
  stage: package
  image: registry.gitlab.com/gitlab-org/gitlab-ci-multi-runner:ci-1.8
  script: 
    - export VERSION=$(cat .VERSION)
    - echo $VERSION
    - *debarch
    - 'eval BIN_ARCH="\${$CI_JOB_NAME}" && export BIN_ARCH'
    - export PACKAGE_ARCH="${CI_JOB_NAME:4}"
    - 'echo " using binary arch $BIN_ARCH"'
    - 'echo " for deb arch: $PACKAGE_ARCH"'
    - rm -rf output/deb && mkdir -pv output/deb
    - > 
       fpm -s dir -t deb
       -n helloworld
       -v ${VERSION}
       -p output/deb/helloworld_${VERSION}_${PACKAGE_ARCH}.deb
       --deb-priority optional --category devel
       --deb-compression bzip2
       --force
       --url https://gitlab.com/deployctl/heloworld_c
       --description "helloworld"
       -m "Gioxa Ltd Inc. <info@gioxa.com>"
       --license "MIT"
       --provides helloworld
       --replaces helloworld
       -a ${PACKAGE_ARCH}
       output/bin/helloworld-${BIN_ARCH}=/usr/bin/helloworld
  artifacts:
    paths:
      - output/deb/

## Package the .deb
deb_i386: *deb
deb_amd64: *deb
deb_armel: *deb
deb_armhf: *deb
deb_arm64: *deb
deb_ppc64le: *deb

# Dynamic environment deployment:

##review Branches
reviewb:
  stage: review
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf release
    - mv -v output/bin release
    - mv -v output/rpm/*.rpm release/
    - mv -v output/deb/*.deb release/
    - deployctl release
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
    on_stop: stop_reviewb
  only:
    - branches
  except:
    - tags
    - master
  tags:
   - deployctl-gioxapp.com

stop_reviewb:
  stage: review
  dependencies: []
  script:
    - deployctl delete
  variables:
    GIT_STRATEGY: none
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  only:
    - branches
  except:
    - tags
    - master
  tags:
    - deployctl-gioxapp.com

## Review Master
reviewm:
  stage: review
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf release
    - mv -v output/bin release
    - mv -v output/rpm/*.rpm release/
    - mv -v output/deb/*.deb release/
    - export DEPLOY_REPO_URL=${MASTER_REPO_URL}
    - 'echo "$DEPLOY_REPO_URL" > release/repository.url'
    - deployctl release
    - rm -rf rpm
    - mkdir rpm
    - cp release/*.rpm rpm/
    - ls -la rpm/*
    - deployctl repo_add    
  environment:
    name: master
    url: http://$CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
    on_stop: stop_reviewm
  only:
    - master
  tags:
    - deployctl-gioxapp.com

stop_reviewm:
  stage: review
  dependencies: []
  script:
    - deployctl delete
  variables:
    GIT_STRATEGY: none
  when: manual
  environment:
    name: master
    action: stop
  only:
    - master
  tags:
    - deployctl-gioxapp.com

## Staging
Release_Staging:
  stage: staging
  environment:
    name: staging
    url: http://staging.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf release
    - mv -v output/bin release
    - mv -v output/rpm/*.rpm release/
    - mv -v output/deb/*.deb release/
    - deployctl release
  only:
    - tags
  except:
    - branches
  tags:
    - deployctl-gioxapp.com

## Production
Release_production:
  stage: production
  variables:
    GIT_STRATEGY: none
  environment:
    name: production
    url: http://$DEPLOY_DOMAIN/$CI_COMMIT_REF_SLUG
  script:
    - rm -rf release
    - mv  output/bin release
    - mv  output/rpm/*.rpm release/
    - mv  output/deb/*.deb release/
    - export DEPLOY_REPO_URL=${MAIN_REPO_URL}
    - 'echo "$DEPLOY_REPO_URL" > release/repository.url'
    - deployctl release
    - rm -rf rpm && mkdir rpm
    - rm -rf deb && mkdir deb
    - cp release/*.rpm rpm/
    - cp release/*.deb deb/
    - deployctl repo_add
  only:
    - tags
  except:
    - branches
  tags:
    - deployctl-gioxapp.com
  when: manual
